import React from 'react';
import Activeimage from './activeimage'
import GridBox from './GridBox';
export default class Tumbnails extends React.Component{

    state={
        Dataimage:[
            {img:"https://images.unsplash.com/photo-1559754417-c1304ca2f80f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",title:"Village one" ,info:"VillageVillageVillageVillage"},
            {img:"https://images.unsplash.com/photo-1587209900507-b13102719674?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",title:"Village two" ,info:"VillageVillageVillageVillage"},
            {img:"https://images.unsplash.com/photo-1588843717625-af356b99172c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",title:"Village three" ,info:"VillageVillageVillageVillage"},
            {img:"https://images.unsplash.com/photo-1530311583484-ea8bf4c407fb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",title:"Village for" ,info:"VillageVillageVillageVillage"},

        

            {img:"https://images.unsplash.com/photo-1571959531100-de445f68c26b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",title:"Village five" ,info:"VillageVillageVillageVillage"},
            {img:"https://images.unsplash.com/photo-1588418000007-b6e4358ea811?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",title:"Village six" ,info:"VillageVillageVillageVillage"},
            {img:"https://images.unsplash.com/photo-1581327190774-940800cb42d5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",title:"Village seven" ,info:"VillageVillageVillageVillage"},
            {img:"https://images.unsplash.com/photo-1594211099212-6956172a833c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",title:"Village eight" ,info:"VillageVillageVillageVillage"},

        ],
        imageindex:0
    }

    attrhandle=(e)=>{
           let attr=e.target.getAttribute("abas");
           this.setState({imageindex:attr})
    }

    renderhandle=()=>{
        let {imageindex,Dataimage}=this.state;

        if(Dataimage.length){
            return(
                <>

                   <h1>{Dataimage[imageindex].title}</h1>

                  <p>{Dataimage[imageindex].info}</p>

                </>
            )
        }
    }


    render(){
        return(
            <div style={stylethumbnail}>
               <div style={{flex:1,background:"#a0f6d2"}}>
                 <Activeimage img={this.state.Dataimage[this.state.imageindex]}/>
                 <GridBox click1={this.attrhandle} img={this.state.Dataimage} />
               </div>


               <div style={dicright}>
                 {this.renderhandle()}               
               </div>
            </div>
        )
    }
}

const stylethumbnail={
    
    background:"#03414d",
    width:"1024px",
    height:"500px",
    borderRadius:"20px",
    display:"flex",
    margin:"20px auto",
    
}

const dicright={
    paddingTop:"200px",
    flex:1,
    color:"#e6f8f6",
    background:"#03414d",
    textAlign:"center",

}