import React from 'react';

const Activeimage=({img})=>{
   return(
       <div style={styleActiveimage}>
          <img src={img.img} width="100%" height="100%" ></img>
       </div>
   )
}

const styleActiveimage={
     background:"#03414d",
    width:"100%",
    height:"65%",
}
export default Activeimage;